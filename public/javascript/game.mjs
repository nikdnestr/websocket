const username = sessionStorage.getItem('username');
if (!username) {
  window.location.replace('/login');
}

const socket = io('', { query: { username: username } });

const userList = document.getElementById('user-list');
const gameArea = document.getElementById('game-area');
const textArea = document.getElementById('text-area');
const quitButton = document.getElementById('quit-btn');

const makeReadyButton = (button) => {
  button.addEventListener('click', () => {
    socket.emit('USER_READY', username);
    readyButton.textContent = 'unready';
    makeUnreadyButton(button);
  });
  return button;
};

const makeUnreadyButton = (button) => {
  button.addEventListener('click', () => {
    socket.emit('USER_UNREADY', username);
    readyButton.textContent = 'ready';
    makeReadyButton(button);
  });
};

const readyButton = document.createElement('button');
readyButton.classList.add('btn');
readyButton.id = 'ready-btn';
readyButton.textContent = 'ready';
makeReadyButton(readyButton);
gameArea.appendChild(readyButton);

quitButton.addEventListener('click', () => {
  socket.emit('disconnect');
  window.location.replace('/login');
  sessionStorage.removeItem('username');
});

socket.on('NEW_USER', (users) => {
  userList.textContent = '';
  for (let i = 0; i < users.length; i++) {
    const user = users[i];
    const container = document.createElement('li');
    const html = `
      <h3><div id="${user.name}-ready" class="ready ready-status-red"></div>${user.name}</h3>
        <div class="bar">
        <div id="${user.name}s-progress" class="progress" style="width: 0px"></div>
      </div>
    `;
    container.innerHTML = html;
    userList.appendChild(container);
  }
});

socket.on('MADE_PROGRESS', (user) => {
  let bar = document.getElementById(`${user.name}s-progress`);
  bar.style.width = `${user.progress}%`;
});

socket.on('GAME_WON', (users) => {
  console.log(users);
  users.sort((a, b) => {
    return a.progress - b.progress;
  });
  alert(`1.${users[0].name}`);
});

socket.on('USER_READY', (user) => {
  const ready = document.getElementById(`${user.name}-ready`);
  ready.classList.replace('ready-status-red', 'ready-status-green');
});

socket.on('USER_UNREADY', (user) => {
  const ready = document.getElementById(`${user.name}-ready`);
  ready.classList.replace('ready-status-green', 'ready-status-red');
});

socket.on('USER_EXISTS', () => {
  window.location.replace('/login');
  sessionStorage.removeItem('username');
  alert('Username with such name already exists');
});

socket.on('GAME_START', (time) => {
  const readyButton = document.getElementById('ready-btn');
  readyButton.classList.add('display-none');
  const timerElement = document.createElement('dvi');
  timerElement.classList.add('center');
  timerElement.id = 'timer';
  gameArea.appendChild(timerElement);
  let timer = setInterval(function () {
    if (time <= 0) {
      clearInterval(timer);
      socket.emit('TIMER_END');
      document.getElementById('timer').classList.add('display-none');
    } else {
      document.getElementById('timer').innerHTML = time;
    }
    time -= 1;
  }, 500);
});

socket.on('TIMER_END', ({ data, time }) => {
  const text = document.createElement('div');
  text.classList.add('center');
  const rand = data[Math.floor(Math.random() * data.length)];
  const words = rand.split('');
  for (let i = 0; i < words.length; i++) {
    let word = words[i];
    let element = document.createElement('span');
    element.id = `word${i}`;
    element.textContent = word;
    text.appendChild(element);
  }
  textArea.classList.remove('display-none');
  textArea.appendChild(text);
  let num = 0;
  document.addEventListener('keydown', function (e) {
    if (num < rand.length) {
      if (e.key === rand[num]) {
        let progress = ((num + 1) / rand.length) * 100;
        document.getElementById(`word${num}`).classList.add('bg-green');
        num++;
        let prog = { name: username, progress: progress };
        socket.emit('MADE_PROGRESS', prog);
      } else {
        document.getElementById(`word${num}`).classList.add('bg-red');
      }
    }
  });
  const gameTimer = document.createElement('span');
  gameTimer.id = 'game-timer';
  gameArea.appendChild(gameTimer);
  let timer = setInterval(function () {
    if (time <= 0) {
      clearInterval(timer);
      socket.emit('GAME_END');
      document.getElementById('game-timer').classList.add('display-none');
    } else {
      document.getElementById('game-timer').innerHTML =
        time + ' seconds remaining';
    }
    time -= 1;
  }, 500);
});
