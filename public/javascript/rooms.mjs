const username = sessionStorage.getItem('username');
if (!username) {
  window.location.replace('/login');
}

const socket = io('', { query: { username: username } });

const gamePage = document.getElementById('game-page');
const roomsPage = document.getElementById('rooms-page');
const roomBtn = document.getElementById('create-room');
const rooms = document.getElementById('rooms');

roomBtn.onclick = () => {
  const name = prompt('Enter room name');
  socket.emit('room create', name);
  createGame(name);
};

socket.on('rooms update', (rooms) => {
  console.log(rooms);
  for (let i = 0; i < rooms.length; i++) {
    createRoom(rooms[i]);
  }
});

socket.on('room create', (name) => {
  createRoom(name);
});

socket.on('room entered', (room) => {
  createGame(room.name, room.users);
});

const createRoom = (room) => {
  const roomContainer = document.createElement('div');
  roomContainer.classList.add('room');
  const users = room.users.join(' ');
  const html = `
    <h2>${room.name}</h2> <span class="user-number">Users: ${users}</span>
    <button class="join button">Join Room</button>
  `;
  roomContainer.innerHTML = html;
  rooms.appendChild(roomContainer);
  const joinButtons = document.getElementsByClassName('join');
  for (let i = 0; i < joinButtons.length; i++) {
    joinButtons[i].addEventListener('click', () => {
      socket.emit('room entered', room.name);
    });
  }
};

const createGame = (roomName, roomUsers = [sessionStorage.username]) => {
  roomsPage.classList.add('display-none');
  gamePage.classList.remove('display-none');
  const users = roomUsers.join(' ');
  const gameContainer = document.createElement('div');
  gameContainer.classList.add('container');
  const html = `
    <h1>${roomName}</h1>
    <h2>${users}</h2>
    <h1 id="msg">test string</h1>
    <h1 id="new"></h1>
    <div style="border: black 1px solid">
      <div
        id="progress"
        style="height: 20px; width: 0px; background-color: red"
      ></div>
    </div>
  `;
  gameContainer.innerHTML = html;
  gamePage.appendChild(gameContainer);
};
