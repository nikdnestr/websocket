import * as config from './config';
import { texts } from '../data';
import User from './User';
const users = new User();

export default (io) => {
  io.on('connection', (socket) => {
    const username = socket.handshake.query.username;
    users.getUser(username)
      ? socket.emit('USER_EXISTS')
      : users.addUser({ name: username, status: false, progress: 0 });
    io.emit('NEW_USER', users.getUsers());
    socket.on('disconnect', () => {
      users.removeUser(username);
    });
    socket.on('MADE_PROGRESS', (user) => {
      users.updateUserProgress(user.name, user.progress);
      io.emit('MADE_PROGRESS', user);
      if (users.getUserProgress(user.name) === 100) {
        socket.local.emit('GAME_WON', users.getUsers());
      }
    });
    socket.on('USER_READY', (name) => {
      users.updateUserStatus(name, true);
      if (users.getAllStatus() === true) {
        io.emit('GAME_START', config.SECONDS_TIMER_BEFORE_START_GAME);
      }
      io.emit('USER_READY', users.getUser(name));
    });
    socket.on('USER_UNREADY', (name) => {
      users.updateUserStatus(name, false);
      io.emit('USER_UNREADY', users.getUser(name));
    });
    socket.on('TIMER_END', () => {
      socket.local.emit('TIMER_END', {
        data: texts,
        time: config.SECONDS_FOR_GAME,
      });
    });
    socket.on('GAME_END', () => {
      socket.local.emit('GAME_WON', users.getUsers());
    });
    // socket.local.emit('rooms update', updateRooms(rooms));
    // socket.on('room create', (name) => {
    //   const room = { name: name, users: [username] };
    //   rooms.push(room);
    //   io.emit('room create', room);
    // });
    // socket.on('room entered', (name) => {
    //   rooms = addUser(name, username);
    //   socket.join(name);
    //   socket.local.emit('room entered', getRoom(name));
    // });
  });
};

// const updateRooms = (rooms) => {
//   const appropriateRooms = [];
//   for (let i = 0; i < rooms.length; i++) {
//     if (rooms[i].users < config.MAXIMUM_USERS_FOR_ONE_ROOM) {
//       appropriateRooms.push(rooms[i]);
//     }
//   }
//   return appropriateRooms;
// };

// const addUser = (room, user) => {
//   return rooms.map((item) => {
//     if (item.name === room) {
//       let newRoom = item;
//       newRoom.users.push(user);
//       return newRoom;
//     }
//     return { ...item };
//   });
// };

// const getRoom = (name) => {
//   for (let i = 0; i < rooms.length; i++) {
//     if (rooms[i].name === name) {
//       return rooms[i];
//     }
//     return null;
//   }
// };
